<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('api', function ($data, $status = 200) {
            return Response::json([
                'status' => $status,
                'message' => ResponseServiceProvider::getResponseMessages($status),
                'data' => $data
            ])
                ->setStatusCode($status);
        });
    }

    static function getResponseMessages($status): string
    {
        return [
            200 => 'ok',
            201 => 'created',
            404 => 'not found',
            400 => 'bad request',
            401 => 'unauthorized',
            403 => 'access denied',
            500 => 'something went wrong'
        ][$status];
    }
}
