<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class RespondingUnit extends Model
{
    use HasFactory;

    protected $fillable = [
        'emergency_id',
        'unit_id'
    ];

    public function emergency(): BelongsTo
    {
        return $this->belongsTo('emergency_id');
    }

    public function unit(): HasOne
    {
        return $this->hasOne('unit_id');
    }
}
