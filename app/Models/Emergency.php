<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Emergency extends Model
{

    public const PRIORITY_UNKNOWN = 0;
    public const PRIORITY_HIGH = 1;
    public const PRIORITY_MEDIUM = 2;
    public const PRIORITY_LOW = 3;

    public const SERVICE_FIRE_DEPT = 'FIRE';
    public const SERVICE_AMBULANCE = 'AMBULANCE';

    use HasFactory;

    protected $fillable = [
        'message',
        'raw_message',
        'priority',
        'city',
        'region',
        'service'
    ];

    public function units(): HasManyThrough
    {
        return $this->hasManyThrough(Unit::class, RespondingUnit::class, 'emergency_id', 'id', 'id', 'unit_id');
    }

    public function getPriorityNameAttribute(): string
    {
        return [
            self::PRIORITY_HIGH => 'spoed',
            self::PRIORITY_MEDIUM => 'gepaste spoed',
            self::PRIORITY_LOW => 'geen spoed',
        ][$this->getAttribute('priority')];
    }
}
